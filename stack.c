#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "stack.h"

int stack_init(stack_t * stack){
  if(NULL == stack){
    return (-1);
  }
  memset(stack->data, '\0', STACK_MAX_SIZE * sizeof(char));
  stack->start = 0;
  stack->current = 0;
}
int stack_push(stack_t * stack, char c){
  if(NULL == stack && c == null){
    return (-1);
  }
  if(0 == stack->current){
    stack->data[0] = c;
  }
  else{
    stack->data[stack->current - stack->start] = c;
  }
  stack->current++;
  stack->data[stack->current] = '\0';
}
int stack_pop(stack_t *stack){
  if(NULL == stack){
    return (-1);
  }
  stack->data[stack->current - 1] = '\0';
  stack->current -= 1;
}
int stack_print(stack_t *stack){
  if(NULL == stack){
    return (-1);
  }
  printf("%s\n", stack->data);
  // for(int i = 0; i < stack->current; i++){
  //   printf("%c ", stack->data[i]);
  // }
  printf("\n");
}
char stack_return_top(stack_t *stack){
  if(NULL == stack){
    return (-1);
  }
  return(stack->data[stack->current - 1]);  
}
int stack_clear(stack_t * stack){
  memset(stack, 0x0, sizeof(stack_t));
}
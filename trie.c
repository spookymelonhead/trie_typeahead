#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "trie.h"
#include "stack.h"

// init tree node, mem allocation, sets c to '\0', reset leaf and sets pointers to NULL
int trie_init(struct trie_s **root_ptr, char c){
  if(NULL == root_ptr || NULL != *(root_ptr)){
    return (-1);
  }
  //memory allocate
  *(root_ptr) = (struct trie_s *)malloc(sizeof(struct trie_s));
  if(NULL == *(root_ptr))
  {
    return (-2);
  }
  // initialize root
  (*root_ptr)->c = c;
  (*root_ptr)->leaf = false;
  for(int i=0; i < TRIE_NUM_OF_CHAR; i++){
    (*root_ptr)->node_list[i] = NULL;
  }
  return (0);
}

extern char matches[TRIE_MAX_NUM_OF_MATCHES][DATASET_MAX_CHARACTER_COUNT];
extern int match_count;

int trie_print_node_all(struct trie_s *root, char *carry){
  stack_t *_carry;

  if(NULL == carry){
    return (-1);
  }
  stack_init(_carry);

  int i=0;
  while(carry[i] != '\0'){
    printf("Pushing user carry to stack %c\n", carry[i]);
    stack_push(_carry, carry[i]);
    i++;
  }

  return (__trie_print_node_all(root, _carry));
}
int __trie_print_node_all(struct trie_s *root, stack_t *carry){
  #ifdef DEBUG
    static int rec_counter;
    rec_counter++;
    // printf("recusvie counter%d\n", rec_counter);
  #endif

  if(NULL == root){
    // printf("Node NULL\n");
    return (-1);
  }
  if(null != root->c){
    // printf("Pushing carry %c\n", root->c);
    stack_push(carry, root->c);
    if(true == root->leaf){
      //push data to some storage here
      if(match_count < TRIE_MAX_NUM_OF_MATCHES){
        strcpy(matches[match_count], carry->data);
        match_count++;
      }
      else{
        printf("Matches overflow\n");
      }
      // stack_print(carry);
    }
  }
  else{
    printf("Root\n");
  }
  for(int i = 0; i < TRIE_NUM_OF_CHAR; i++)
  {
    // printf("Index:%d\n", i );
    // stack_print(carry);
    // trie_print_node(root->node_list[i]);
    __trie_print_node_all(root->node_list[i], carry);
  }
  //pop carry
  stack_pop(carry);
  return (0);
}
int trie_print_node(struct trie_s *root){
  if(NULL == root){
    return (-1);
  }
  if(null == root->c){
    printf("Root: null\n");
  }
  else{
    printf("Root:%c\n", root->c);
  }

  for(int i = 0; i < TRIE_NUM_OF_CHAR; i++ ){
    if(NULL != root->node_list[i]){
      printf("%c\n", root->node_list[i]->c);
    }
    else{
      // printf("null\n");
    }
  }
}
struct trie_s * trie_search_char(struct trie_s *root, char query){
  if(NULL == root){
    return (NULL);
  }
  #ifdef _TRIE_SEARCH_
    if (NULL != root->node_list[(int)query])
    {  
      int jack;
      if(query == ' '){
        jack = TRIE_NUM_OF_CHAR - 1U;
      }
      else{
        jack = (int)query - (int)'a' - 1;
      }

      trie_print_node(root->node_list[jack]);
      // printf("Index check: %d\n", jack);
      return (root->node_list[jack]);
    }
  #else
    for(int i = 0; i < TRIE_NUM_OF_CHAR; i++){
      if(root->node_list[i] != NULL)
      {
        if(query == root->node_list[i]->c){
          //Found
          return (root->node_list[i]);
        }
      }
      else{
        //Pass
      }
    }
    return (NULL);
  #endif
}

/*
  Inserts new node to the argument(root) node with c argument(val)
*/
struct trie_s * __trie_insert_node(struct trie_s *root, char val){
  if(NULL == root || null == val)
  {
    return (NULL);
  }

  int i = TRIE_NUM_OF_CHAR - 1;
  // printf("Inserting new node with val: %c\n", val);
  if(' ' == val){
  }
  else{
    i = (int)val - (int)'a';
  }
  trie_init(&(root->node_list[i]), val);
  // trie_print_node(root->node_list[i]);

  return root->node_list[i];
}
/*
  Sets leaf flag to true for a argument(node)
  Validation:
    check if node is NULL
      yes: return
    set leaf to true
*/
int trie_mark_node_as_leaf(struct trie_s *node){
  if(NULL == node){
    return (-1);
  }

  if(false == node->leaf){
    node->leaf = true;
    printf("Marked as leaf: %c\n", node->c);
  }
  else
  {
    printf("Already a leaf: %c\n", node->c);
  }
}
/*
  To insert word in Trie, recursively
  validations:
    check if argument(node) is NULL or argument(word) is NULL
      Yes-> return

  Local var used:
    word_counter- counter to traverse through the characters in the word
    searched_node- pick character from the word, find it in the trie and put it in searched node
    new_node- if character does not exist, ie reached end of the branch then make a new node put into this

  Algorithm:
    traverse through characters in the word
      if c is not '\0'
        searched_node = search charater in the trie
      if NULL is not searched_node
        if next character in the word is '\0'
          mark searched node as leaf
      else
        insert new node in trie
        if next character in the word is '\0'
          mark searched node as leaf
      count++
      recursive call(with rest of the characters in string as argument)
*/
int trie_insert_word(struct trie_s *node, char *word){
  #ifdef DEBUG
    static int rec_counter;
    rec_counter++;
  // printf("Recusion count: %d\n", rec_counter);
  // printf("Here %c\n", word[counter]);
  #endif /* DEBUG */

  int counter = 0;
  struct trie_s *temp_node = NULL;
  
  if(NULL == node){
    return (-1);
  }
  if(NULL == word){
    return (-2);
  }

  if(null != word[counter]){
    temp_node = trie_search_char(node, word[counter]);
  }
  else{
    return (-1);
  }
  // Found Char
  if(NULL != temp_node){
    //If next character is null, that means end of the word mark last character as leaf
    #ifdef DEBUG_TRIE_INSERT
      printf("Inserting word %s found %c\n", word, temp_node->c);
    #endif
    if('\0' == word[counter + 1]){
      trie_mark_node_as_leaf(temp_node);
      return (0);
    }
  }
  // Not Found
  else{
    #ifdef DEBUG_TRIE_INSERT
      printf("Inserting %s Charactor %c not found in search.\n", word, word[counter]);
    #endif
    temp_node = __trie_insert_node(node, word[counter]);
    if(NULL != temp_node){
      if('\0' == word[counter + 1]){
        trie_mark_node_as_leaf(temp_node);
        return (0);   
      }
    }
    else{
      printf("Broke here\n");
      return -2;
    }
  }
  counter++;
  check_success(trie_insert_word(temp_node, &word[counter]));
  return (0);
}

/*
  Search word in Trie
  Validation:
    check if argument(node) or argument(word) is NULL 
      Yes: return
  Local variables:
    results: stores 
    result_pointer:
    count to traverse throught the word
    traverser to traverse throught trie, 
    traverser = node
    size stores size of the arguemnt(word) for traversing

    loop to traverse through every character(c) in argument (word)
      traverser = trie_search(traverser, c)
      if traverser is not NULL
        push(traverser->c in stack)
      else
        look ahead for matches
*/
int trie_search_word(struct trie_s *node, char *word){
  if(NULL == node || NULL == word){
    return (-1);
  }

  static stack_t results;
  stack_init(&results);

  int count = 0;
  struct trie_s *traverser = node;
  struct trie_s *last_traverser = NULL;
  const int size = strlen(word);
  
  while(count < size){
    printf("searching character %c in traverser %c\n", word[count], traverser->c);
    
    if(traverser != NULL){
      last_traverser = traverser;
    }
    traverser = trie_search_char(traverser, word[count]);

    if(NULL == traverser)
    {
      printf("Match ended with traverser NULL, No Match found!\n\n");
      // stack_print(&results);
      stack_pop(&results);
      printf("Last non null traverser %c\n", last_traverser->c);
      __trie_print_node_all(last_traverser, &results);
      return (0);
    }
    else{
      if(count == size - 1){
        printf("Match ended with traverser !NULL Exact Match found!\n\n");
        // stack_print(&results);
        __trie_print_node_all(traverser, &results);
        return (0);
      }
      else{
        // printf("here\n");
        stack_push(&results, traverser->c);
      }
    }
    count++;
  }
}
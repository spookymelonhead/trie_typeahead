#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define null '\0'
#define DATASET_MAX_CHARACTER_COUNT (30U)

void check_success(int val);
char * convert_char_to_string(char c);

#endif /* _COMMON_H_ */
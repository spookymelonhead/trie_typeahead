#ifndef _TRIE_H_
#define _TRIE_H_

#include "common.h"
#include "stack.h"

// 0-26(a-z) 27(' ')
#define TRIE_NUM_OF_CHAR (27U)
#define TRIE_MAX_NUM_OF_MATCHES (5U)

/* Trie Struct */
struct trie_s{
  // character
  char c;
  // is it a leaf? 
  bool leaf;
  // Array of trie pointer
  struct trie_s *node_list[TRIE_NUM_OF_CHAR];
};

// init tree node, sets c to '\0' and pointers to NULL
int trie_init(struct trie_s **root_ptr, char c);
int __trie_print_node_all(struct trie_s *root, stack_t *carry);
int trie_print_node_all(struct trie_s *root, char *carry);
int trie_print_node(struct trie_s *root);
struct trie_s * trie_search_char(struct trie_s *root, char query);
struct trie_s * __trie_insert_node(struct trie_s *root, char val);
int trie_insert_word(struct trie_s *node, char *word);
int trie_search_word(struct trie_s *node, char *word);

#endif /* _TRIE_H_ */
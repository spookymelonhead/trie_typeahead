#ifndef _STACK_H_
#define _STACK_H_

#include "common.h"
// Stack Max size
#define STACK_MAX_SIZE (100U)

typedef struct{
  char data[STACK_MAX_SIZE];
  int start;
  int current;
}stack_t;

int stack_init(stack_t * stack);
int stack_push(stack_t * stack, char c);
int stack_pop(stack_t *stack);
int stack_print(stack_t *stack);
char stack_return_top(stack_t *stack);
int stack_clear(stack_t * stack);
#endif /* _STACK_H_ */
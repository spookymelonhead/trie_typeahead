#include "common.h"
#include "stack.h"
#include "trie.h"

#define DATASET_COUNT (50U)
char dataset[DATASET_COUNT][DATASET_MAX_CHARACTER_COUNT] ={"c",
                      "data structures",
                      "random",
                      "comic",
                      "general",
                      "technology",
                      "life lessons",
                      "python",
                      "machine learning",
                      "linux",
                      "science fiction",
                      "scifi",
                      "music",
                      "marketing",
                      "food",
                      "physics",
                      "programming",
                      "django",
                      "flask",
                      "java",
                      "quantum mechanics",
                      "chemistry",
                      "mathematics",
                      "amazing",
                      "mind bending",
                      "intuition",
                      "science in everyday",
                      "avocado",
                      "lucid",
                      "dreaming",
                      "movies",
                      "are we in a simulation",
                      "motorcycles",
                      "ai",
                      "porcupine tree",
                      "oranges",
                      "simplified",
                      "core",
                      "rabbits foot",
                      "dig deeper",
};

char matches[TRIE_MAX_NUM_OF_MATCHES][DATASET_MAX_CHARACTER_COUNT];
int match_count = 0;
void clear_matches(){
  for(int i = 0; i < TRIE_MAX_NUM_OF_MATCHES; i++){
    memset(matches[i], 0x0, sizeof(char)* DATASET_MAX_CHARACTER_COUNT);
  }
  match_count = 0;
}
int main(int argc, char ** argv){
  if(argc < 2){
    fprintf(stderr, "Usage: Requires query argument.\n");
    return (-1);
  }
  // stack_init(&results);

  printf("%s\n", "start");
  struct trie_s *root = NULL;
  check_success(trie_init(&root, null));

  int i=0;
  while(i < DATASET_COUNT){
    check_success(trie_insert_word(root, dataset[i]));
    i++;
  }
  // trie_print_node(root);
  // trie_print_node(root->node_list[2]->node_list[0]);
  // trie_print_node(root->node_list[9]);
  // trie_print_node(root->node_list[9]->node_list[0]);

  // stack_t carry;
  // stack_init(&carry);
  // check_success(trie_print_node_all(root, &carry));
  char q[DATASET_MAX_CHARACTER_COUNT];
  while(true){
    printf("q:");
    scanf("%s", q);
    trie_search_word(root, q);
    for(i = 0; i< match_count; i++){
      printf("%s\n", matches[i]);
    }
    clear_matches();
    memset(q, 0x0, sizeof(char)* DATASET_MAX_CHARACTER_COUNT);
  }
  // trie_print_node(root->node_list[11]->node_list[8]->node_list[13]->node_list[20]->node_list[23]);
  // trie_print_node(root->node_list[11]->node_list[8]->node_list[13]->node_list[20]->node_list[23]);
  // check_success(trie_print_node_all(root->node_list[11], ""));

  //stack test routinest
  // stack_t carry;
  // stack_init(&carry);
  // stack_push(&carry, 'c');
  // stack_push(&carry, 'a');
  // stack_push(&carry, 't');
  // stack_push(&carry, 'e');
  
  // stack_print(&carry);
  // stack_pop(&carry);
  
  // stack_print(&carry);
  // stack_pop(&carry);
  // stack_pop(&carry);
  // stack_print(&carry);
  // printf("%c\n", stack_return_top(&carry)) ;
  return 0;
}